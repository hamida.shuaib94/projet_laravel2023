<?php

namespace Database\Seeders;

use App\Models\Course;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $courses = [
            ['name' => 'Base des réseaux', 'code' => '1351'],
            ['name' => 'Environnement et technologies du web', 'code' => '1352'],
            ['name' => 'SGBD (Système de gestion de bases de données)', 'code' => '1353'],
            ['name' => 'Création de sites web statiques', 'code' => '1354'],
            ['name' => 'Approche Design', 'code' => '1355'],
            ['name' => 'CMS - niveau 1', 'code' => '1356'],
            ['name' => 'Initiation à la programmation', 'code' => '1357'],
            ['name' => 'Activités professionnelles de formation', 'code' => '1358'],
            ['name' => 'Scripts clients', 'code' => '1359'],
            ['name' => 'Scripts serveurs', 'code' => '1360'],
            ['name' => 'Framework et POO côté Serveur', 'code' => '1361'],
            ['name' => 'Projet Web dynamique', 'code' => '1362'],
            ['name' => 'Veille technologique', 'code' => '1363'],
            ['name' => 'Epreuve intégrée', 'code' => '1364'],
            ['name' => 'Anglais UE1', 'code' => '1783'],
            ['name' => 'Anglais UE2', 'code' => '1784'],
            ['name' => 'Initiation aux bases de données', 'code' => '1440'],
            ['name' => 'Principes algorithmiques et programmation', 'code' => '1442'],
            ['name' => 'Programmation orientée objet', 'code' => '1443'],
            ['name' => 'Web : principes de base', 'code' => '1444'],
            ['name' => 'Techniques de gestion de projet', 'code' => '1448'],
            ['name' => 'Principes d’analyse informatique', 'code' => '1449'],
            ['name' => 'Eléments de statistique', 'code' => '1755'],
            ['name' => 'Structure des ordinateurs', 'code' => '1808'],
            ['name' => 'Gestion et exploitation de bases de données', 'code' => '1811']
        ];

        foreach ($courses as $course) {
            Course::create($course);
        }
    }
}
