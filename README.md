
# Création d'un utilisateur administrateur

Pour faciliter la gestion de l'application, nous avons mis en place une commande artisan pour créer un utilisateur administrateur.

## Comment l'utiliser :

1. **Ouvrir le terminal :** 
   Lancez votre terminal ou votre invite de commande.

2. **Se déplacer vers le répertoire du projet :** 
   Naviguez vers le répertoire de votre projet Laravel.

3. **Exécuter la commande :** 
   Tapez la commande suivante:

   php artisan create-admin
### Fournir les informations :

1. Nom d'utilisateur : Lorsque vous y êtes invité, renseignez un nom d'utilisateur.
2. Adresse e-mail : Ensuite, fournissez une adresse e-mail valide.
3. Mot de passe : Pour des raisons de sécurité, lors de la saisie du mot de passe, aucune frappe ne sera affichée à l'écran. C'est une mesure courante pour s'assurer que le mot de passe reste confidentiel pendant la saisie. Renseignez simplement votre mot de passe et appuyez sur "Entrée".

### Confirmation :
Une fois la commande exécutée, un message s'affichera pour confirmer la création de l'utilisateur administrateur("Admin user created successfully.").