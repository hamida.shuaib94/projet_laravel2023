@extends('layouts.app')

@section('content')
<h1>Liste des utilisateurs</h1>

<table class="table table-bordered table-hover">
    <thead class="thead-light">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Username</th>
            <th scope="col">Email</th>
            <th scope="col">Date de création</th>
            <th scope="col">Dernière connexion</th>
            <th scope="col">Rôle</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->username }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ date('d/m/Y', strtotime($user->created_at)) }}</td>
            <td>{{ date('d/m/Y', strtotime($user->lastlogin)) }}</td>
            <td>
                @if($user->isAdmin())
                    Admin
                @elseif($user->isProfessor())
                    Professeur
                @elseif($user->isStudent())
                    Étudiant
                @else
                    Visiteur
                @endif
            </td>
            <td>
                @if(Auth::id() != $user->id) <!-- Ne pas montrer le formulaire pour l'admin lui-même -->
                <form action="{{ route('user.changerole', $user->id) }}" method="post" style="display: inline;">
                    @csrf
                    <select name="role" onchange="this.form.submit()">
                        <option value="4" {{ $user->isVisitor() ? 'selected' : '' }}>Visiteur</option>
                        <option value="3" {{ $user->isStudent() ? 'selected' : '' }}>Étudiant</option>
                        <option value="2" {{ $user->isProfessor() ? 'selected' : '' }}>Professeur</option>
                    </select>
                </form>

                <form action="{{ route('user.destroy', $user->id) }}" method="POST" style="display: inline;">
                    @csrf
                    @method('DELETE')
                    <button type="submit" onclick="return confirm('Êtes-vous sûr de vouloir supprimer cet utilisateur?');">Supprimer</button>
                </form>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
