@extends('layouts.app')

@section('content')
<h1 class="text-center">Liste des cours</h1>
<table class="table table-bordered table-striped table-hover">
    <thead class="thead-dark">
        <tr>
            <th>Code</th>
            <th>Nom du cours</th>
        </tr>

    </thead>
    <tbody>
        @foreach($courses as $course)
        <tr>
            <td>{{ $course->name }}</td>
            <td>{{ $course->code }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection