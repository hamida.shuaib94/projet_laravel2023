@extends('layouts.app')

@section('content')

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-6">

            <div class="card border-0 shadow-lg">
                <div class="card-body">
                    <h3 class="text-center text-primary mb-4">Créer un compte</h3>

                    @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </div>
                    @endif

                    <form action="{{ route('register.process') }}" method="post">
                        @csrf

                        <div class="form-group mb-4">
                            <input type="text" name="username" id="username" class="form-control rounded-pill" placeholder="Identifiant" required>
                        </div>

                        <div class="form-group mb-4">
                            <input type="password" name="password" id="password" class="form-control rounded-pill" placeholder="Mot de Passe" required>
                        </div>

                        <div class="form-group mb-4">
                            <input type="email" name="email" id="email" class="form-control rounded-pill" placeholder="Email" required>
                        </div>

                  

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary btn-block rounded-pill">S'inscrire</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection