@extends('layouts.app')

@section('content')
<h1 class="mb-4">Page Profil</h1>
<table class="table table-bordered table-hover">
    <thead class="thead-light">
        <tr>
            <th scope="col">Attribut</th>
            <th scope="col">Valeur</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>Identifiant</th>
            <td>{{ $user->username }}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{ $user->email }}</td>
        </tr>
        <tr>
            <th>Création</th>
            <td>{{ \Carbon\Carbon::parse($user->created)->format('d/m/Y') }}</td>
        </tr>
        <tr>
            <th>Dernière visite</th>
            <td>{{ \Carbon\Carbon::parse($user->lastlogin)->format('d/m/Y H:i') }}</td>
        </tr>
        <tr>
    <th>Role</th>
    <td>
        @if($user->isAdmin())
            Admin
        @elseif($user->isProfessor())
            Professeur
        @elseif($user->isStudent())
            Étudiant
        @else
            Visiteur
        @endif
    </td>
</tr>
    </tbody>
</table>
<a href="{{  route('editProfil.index', Auth::user()->id) }}">Modifier mon profil</a>
<hr>
<a href="{{ route('profile.exportJson') }}" class="btn btn-primary">Exporter mes données</a>
@endsection