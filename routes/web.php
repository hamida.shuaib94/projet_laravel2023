<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\StaticController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/', [StaticController::class, 'index'])->name('accueil.index');

Route::get('/courses', [CourseController::class, 'index'])->name('courses.index');

Route::get('/login', [StaticController::class, 'login'])->name('login.index');
Route::post('/login', [AuthController::class, 'processLogin'])->name('login.process');

Route::get('/register', [AuthController::class, 'showRegisterForm'])->name('register.index');
Route::post('/register', [AuthController::class, 'processRegister'])->name('register.process');

Route::middleware(['auth'])->group(function () {
    Route::get('/profil', [UserController::class, 'showProfile'])->name('profil.index');
    Route::get('/profil/{id}/edit', [UserController::class, 'editProfile'])->name('editProfil.index');
    Route::put('/user/{id}/update', [UserController::class, 'updateProfile'])->name('user.update')->where('id', '[0-9]+');
    Route::get('/profile/export-json', [UserController::class, 'exportJson'])->name('profile.exportJson');
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout.index');

    Route::middleware(['admin'])->group(function () {
        Route::get('/users', [UserController::class, 'index'])->name('admin.index');
        Route::post('/user/{id}/changerole', [UserController::class, 'changeRole'])->name('user.changerole');
        Route::delete('user/{id}', [UserController::class, 'destroy'])->name('user.destroy');
    });
});
