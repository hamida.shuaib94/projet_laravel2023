<?php

namespace App\Models;

use Illuminate\Support\Facades\Hash;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'password',
        'email',
        'created',
        'lastlogin',
        'role',
        'image'
    ];




    protected static function boot()
    {
        parent::boot();
        static::creating(function ($user) {
            if (is_null($user->role)) {
                $user->role = 4; // 4 est pour le rôle "visiteur" selon vos méthodes
            }
        });
    }
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    public function isVisitor()
    {
        return $this->role === 4; // Visiteur
    }
    
    public function isStudent()
    {
        return $this->role === 3; // Etudiant
    }
    
    public function isProfessor()
    {
        return $this->role === 2; // Professeur
    }
    
    public function isAdmin()
    {
        return $this->role === 1; // Admin - le premier utilisateur à s'inscrire
    }
    
    /**
     * The attributes that should be hidden for serialization.
     *Définit les attributs qui doivent être cachés lors de la 
     *conversion du modèle en tableau ou en JSON.
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *chaque fois que vous accéderez à l'attribut admin sur une instance du modèle User, vous obtiendrez true ou false (ou null si la valeur est null dans la base de données).
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',

    ];
}
