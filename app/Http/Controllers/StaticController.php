<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Models\Course;
class StaticController extends Controller
{

    public function index()
    {
        return view('welcome');
    }


    public function login()
    {
        return view('login.index');
    }


 

    public function profil()
    {
        return view('profil.index');
    }

    public function admin()
    {
        return view('admin.index');
    }

  
    public function logout()
    {
        return view('logout.index');
    }

 

}