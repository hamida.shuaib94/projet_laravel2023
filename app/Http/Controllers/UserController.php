<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('admin.index', compact('users'));
    }

    public function showProfile()
    {
        $user = Auth::user();
        return view('profil.index', compact('user'));
    }

    public function editProfile($id)
    {
        $user = Auth::user();

        if ($user->id != $id) {
            return redirect()->back()->with('error', 'Accès non autorisé');
        }
        return view('editProfil.index', compact('user'));
    }

    public function updateProfile(Request $request, $id)
    {
        if (Auth::id() != $id) {
            return redirect()->route('accueil.index')->with('error', 'Accès non autorisé');
        }
        $user = User::findOrFail($id);

        if (Auth::id() !== $user->id && !$user->isAdmin()) {
            return redirect()->back()->with('error', 'Accès non autorisé');
        }

        $request->validate([
            'password' => 'nullable|min:8',
            'email' => 'email|unique:users,email,' . $user->id
        ]);

        if ($request->filled('password')) {
            $user->password = $request->password;
        }

        if ($request->filled('email')) {
            $user->email = $request->email;
        }

        $user->save();

        return redirect()->route('profil.index')->with('success', 'Profil mis à jour avec succès.');
    }

    public function exportJson()
    {
        if (!Auth::check()) {
            return redirect()->route('login.index')->with('error', 'Veuillez vous connecter pour exporter vos données.');
        }
           /**
         * @var User $user
         */
   $user = Auth::user();
        $userDataJson = json_encode($user->toArray(), JSON_PRETTY_PRINT);

        return response($userDataJson)
            ->header('Content-Type', 'application/json')
            ->header('Content-Disposition', 'attachment; filename="user_data.json"');
    }

    //... Autres méthodes de UserController déjà existantes ...

    public function changeRole(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $request->validate([
            'role' => 'required|in:4,3,2', // 4: visiteur, 3: étudiant, 2: professeur
        ]);

        $user->role = $request->role;
        $user->save();

        return redirect()->back()->with('success', 'Rôle modifié avec succès.');
    }

    public function destroy(string $id)
    {
        $user = User::findOrFail($id);
        if (Auth::id() == $user->id) {
            return redirect()->route('admin.index')->with('error', 'Vous ne pouvez pas vous supprimer vous-même.');
        }
        $user->delete();
        return redirect()->route('admin.index')->with('success', 'Utilisateur supprimé avec succès.');
    }
}
