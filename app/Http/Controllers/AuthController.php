<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use Log;

class AuthController extends Controller
{

    // Method to process the POST request sent by the login form.
    public function processLogin(Request $request)
    {
        $credentials = $request->only('username', 'password');
        // If authentication is successful
  
        if (Auth::attempt($credentials)) {
            // Update the 'lastlogin' field
            /**
             * @var User $user
             */
            $user = Auth::user();
            $user->lastlogin = now();
            $user->save();

            return redirect()->route('profil.index')->with('message', 'Bienvenue' . $user->username);
        }

        // If authentication fails
        return back()->withErrors([
            'login_error' => "Les informations d'identification fournies sont incorrectes.",
        ]);
    }

    public function showRegisterForm()
    {
        return view('register.index');
    }

    public function processRegister(Request $request)
    {
        // Vérifier si c'est le premier utilisateur
        $isFirstUser = !User::count();
        
        // Définir les règles de validation pour le formulaire
        $validationRules = [
            'username' => 'required|unique:users',
            'password' => 'required|min:8',
            'email' => 'required|email|unique:users',
        ];
        
        // Valider la requête
        $request->validate($validationRules);
    
        // Si c'est le premier utilisateur, il sera admin. Sinon, il sera visiteur par défaut.
        if ($isFirstUser) {
            $roleValue = 1; // Admin
        } else {
            $roleValue = 4; // Visiteur par défaut pour tous les autres
            
   
        }
    
        try {
            // Créer le nouvel utilisateur
            User::create([
                'username' => $request->username,
                'password' => $request->password, // hashed using the mutator in the User model
                'email' => $request->email,
                'role' => $roleValue
            ]);
        
            // Rediriger vers la page de connexion avec un message de succès
            return redirect()->route('login.index')->with('success', 'Inscription réussie. Vous pouvez maintenant vous connecter.');
        } catch (\Exception $e) {
            return back()->with('error', 'Une erreur est survenue. Veuillez réessayer.');
        }
    }
    

    public function logout()
    {
        $user = Auth::user();
        Auth::logout();
        return redirect()->route('accueil.index')->with('message', 'Au revoir ' . $user->username);
    }
}
