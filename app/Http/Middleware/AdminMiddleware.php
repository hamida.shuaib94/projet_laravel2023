<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
     // Vérifie si l'utilisateur n'est pas authentifié.
     if (!Auth::check()) {
        return redirect()->route('accueil.index')->with('error', 'Vous devez être connecté pour accéder à cette page.');
    }
    
        // Si l'utilisateur est authentifié, mais n'a pas le bon rôle.
        if (Auth::user()->role !== 1) {
            return redirect()->route('accueil.index')->with('error', 'Accès non autorisé');
        }
    
        // Si tout va bien, continuez la requête.
        return $next($request);
    }
}    
