<?php

namespace App\Console\Commands;

use Hash;
use Illuminate\Console\Command;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new admin user';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->ask('Enter the admin name');
        $email = $this->ask('Enter the admin email');
        $password = $this->secret('Enter the admin password');



        $user = new \App\Models\User();
        $user->username = $name;
        $user->email = $email;
        $user->password = $password;// Le mutateur se chargera du hachage
        $user->role = 1;  // Supposant que 1 est le rôle d'administrateur
        $user->save();


        $this->info('Admin user created successfully.');
    }
}
